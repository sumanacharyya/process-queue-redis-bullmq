import { Worker } from "bullmq";

const sendEmail = (delay = 5) => {
    return new Promise((resolve, reject) =>
        setTimeout(() => resolve(), delay * 1000)
    );
};

const worker = new Worker("email-queue", async (job) => {
    console.log("Starting worker with job id: ", job.id);
    console.log("With email id: ", job.data.email);
    console.log("The subject: ", job.data.subject);
    console.log("The body: ", job.data.body);
    await sendEmail();
    console.log("Email sent successfully");
});
