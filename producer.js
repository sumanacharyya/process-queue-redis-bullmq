import { Queue } from "bullmq";

const notificationQueue = new Queue("email-queue", {
    connection: {
        host: "127.0.0.1",
        port: "6379",
    },
});

async function init() {
    const resp = await notificationQueue.add("email to Suman", {
        email: "suman@gmail.com",
        subject: "Subject for Email to Suman",
        body: "Hey Suman, welcoming to test email.",
    });
    console.log("Job added to queue: ", resp.id);
}
init();
